import fs from 'fs';
import https from 'https';
import NodeID3 from 'node-id3';
import path from 'path';
import SoundCloud from 'soundcloud-scraper';
import { tempDir } from './config';
import sanitize from 'sanitize-filename';

export const processSong = async (
	client: SoundCloud.Client,
	outDir: string,
	url: string
) => {
	const song = await client.getSongInfo(url);

	const imageUrl = song.thumbnail;
	const imageExt = path.extname(imageUrl);
	const imagePath = path.join(tempDir, song.id + imageExt);
	await downloadThumbnailAsync(imageUrl, imagePath);

	const filePath = path.join(
		outDir,
		`${sanitize(song.author.name)} - ${sanitize(song.title)}.mp3`
	);
	await downloadSongAsync(song, filePath);

	const tags: NodeID3.Tags = {
		title: song.title,
		artist: song.author.name,
		date: song.publishedAt.toISOString().slice(0, 19),
		year: song.publishedAt.getUTCFullYear().toString(),
		image: imagePath,
	};
	await NodeID3.write(tags, filePath);
};

const downloadThumbnailAsync = (url: string, imagePath: string) => {
	return new Promise<void>((resolve, reject) => {
		const file = fs.createWriteStream(imagePath);
		file.on('error', (err) => {
			fs.unlink(imagePath, () => {
				reject(err);
			});
		});
		file.on('finish', () => {
			file.close(() => resolve());
		});

		const req = https.get(url, (response) => {
			if (response.statusCode !== 200) {
				reject('Response status was ' + response.statusCode);
				return;
			}

			response.pipe(file);
		});
		req.on('error', (err) => {
			fs.unlink(imagePath, () => {
				reject(err);
			});
		});
	});
};

const downloadSongAsync = async (song: SoundCloud.Song, filePath: string) => {
	const stream = await song.downloadProgressive();
	const writer = stream.pipe(fs.createWriteStream(filePath));

	writer.on('error', (e) => {
		throw e;
	});

	await new Promise((resolve) => {
		writer.on('finish', resolve);
	});
};
