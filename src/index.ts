import chalk from 'chalk';
import fs from 'fs/promises';
import mkdirp from 'mkdirp';
import rimraf from 'rimraf';
import SoundCloud from 'soundcloud-scraper';
import pkg from '../package.json' assert { type: 'json' };
import { args } from './args';
import { outDir, tempDir } from './config';
import { isUrl } from './isUrl';
import { processSong } from './processSong';

const log = console.log;

log(chalk.whiteBright(`scdl v${pkg.version}`));

// Clean option
if (args.clean) {
	log('Cleaning ' + chalk.whiteBright('out/'));
	await rimraf(outDir);
}

await mkdirp(outDir);
await mkdirp(tempDir);

const urls = args.url ?? [];

const files = args.file ?? [];
for (const file of files) {
	const contents = await fs.readFile(file);
	contents
		.toString()
		.split('\n')
		.forEach((line) => {
			const url = line.trim();
			if (url.length === 0) return;

			if (!isUrl(url)) {
				log(
					chalk.red(
						'Failed to parse url in ' +
							chalk.whiteBright(file) +
							': ' +
							chalk.white(url)
					)
				);
			} else {
				urls.push(url);
			}
		});
}

if (!urls.length) {
	log(chalk.red('Nothing to download. Try --help.'));
	process.exit(1);
}

const urlNumLength = urls.length.toString().length;
const blankPrefix = ' '.repeat(urlNumLength * 2 + 4);

log(blankPrefix + 'Generating key');
const key = await SoundCloud.Util.keygen();
const client = new SoundCloud.Client(key);

let failedUrls: string[] = [];

for (let i = 0; i < urls.length; i++) {
	const prefix = chalk.gray(
		`[${(i + 1).toString().padStart(urlNumLength)}/${urls.length}] `
	);

	const url = urls[i];

	log(prefix + 'Downloading ' + chalk.whiteBright(url));

	try {
		await processSong(client, outDir, url);
	} catch (e) {
		console.error(prefix + chalk.red(e));
		failedUrls.push(url);
	}
}

log(
	blankPrefix +
		'Downloaded ' +
		chalk.whiteBright(`${urls.length - failedUrls.length}/${urls.length}`) +
		' songs to ' +
		chalk.whiteBright('out/')
);

if (failedUrls.length > 0) {
	log('\nThese urls were not downloaded:\n');

	failedUrls.forEach((u) => log(chalk.whiteBright(u)));

	log();
}
