import { parse } from 'ts-command-line-args';

interface Options {
	help: boolean;
	clean: boolean;
	url?: string[];
	file?: string[];
}

export const args = parse<Options>(
	{
		help: {
			type: Boolean,
			alias: 'h',
			defaultValue: false,
			description: 'Show this help message',
		},
		clean: {
			type: Boolean,
			alias: 'c',
			defaultValue: false,
			description: 'Remove previous files (default: false)',
		},
		url: {
			type: String,
			optional: true,
			multiple: true,
			alias: 'u',
			description: 'SoundCloud urls to download',
		},
		file: {
			type: String,
			optional: true,
			multiple: true,
			alias: 'f',
			description: 'File with newline-separated urls to download',
		},
	},
	{
		helpArg: 'help',
		headerContentSections: [
			{
				header: 'scdl',
				content: 'Download tracks from SoundCloud and add ID3 tags',
			},
		],
	}
);
