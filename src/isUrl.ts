export const isUrl = (s: string) => {
	try {
		new URL(s);
		return true;
	} catch (e) {
		return false;
	}
};
