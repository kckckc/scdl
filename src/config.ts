import path from 'path';
import { fileURLToPath } from 'url';

export const outDir = path.resolve(
	path.dirname(fileURLToPath(import.meta.url)),
	'..',
	'out'
);
export const tempDir = path.resolve(
	path.dirname(fileURLToPath(import.meta.url)),
	'..',
	'temp'
);
