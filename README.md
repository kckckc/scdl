# scdl

Downloads songs from SoundCloud and adds ID3 tags for artist, title, date, and cover art.

## Usage

```bash
# Install
$ yarn
$ yarn start --help

# From url(s)
$ scdl --url https://soundcloud.com/... -u https://soundcloud.com/...

# From file(s). Each url should be on a separate line
$ scdl --file urls.txt -f moreurls.txt

# Remove previous files from out/
$ scdl --clean
$ scdl --clean -u ...
```
